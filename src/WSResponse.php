<?php
namespace TkachInc\RFC6455;

/**
 * Class Response
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class WSResponse
{
	protected $close;
	protected $opcode;
	protected $response;
	protected $payloadOffset;
	protected $dataLength;

	public function __construct($response, $close = false, $opcode = null, $dataLength = null)
	{
		$this->response = $response;
		$this->close = $close;
		$this->opcode = $opcode;
		$this->dataLength = $dataLength;
	}

	public function getDataLength()
	{
		return $this->dataLength;
	}

	public function getOpcode()
	{
		return $this->opcode;
	}

	public static function getType($opcode)
	{
		switch ($opcode) {
			// text frame:
			case 1:
				$type = 'text';
				break;

			case 2:
				$type = 'binary';
				break;

			// connection close frame:
			case 8:
				$type = 'close';
				break;

			// ping frame:
			case 9:
				$type = 'ping';
				break;

			// pong frame:
			case 10:
				$type = 'pong';
				break;

			default:
				$type = null;
				break;
		}

		return $type;
	}

	public function getResponse()
	{
		return $this->response;
	}

	public function isClose()
	{
		return $this->close;
	}
}