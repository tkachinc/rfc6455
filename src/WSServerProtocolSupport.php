<?php
namespace TkachInc\RFC6455;

/**
 * Class WsServer
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class WSServerProtocolSupport
{
	/**
	 * @param                     $data
	 * @return WSResponse
	 */
	public static function handshake($data)
	{
		$lines = preg_split("/\r\n/", $data);

		// check for valid http-header:
		if (!preg_match('/\AGET (\S+) HTTP\/1.1\z/', $lines[0], $matches)) {
			return static::makeHttpErrorResponse(400);
		}

		// check for valid application:
		$path = $matches[1];
		// generate headers array:
		$headers = [];
		foreach ($lines as $line) {
			$line = chop($line);
			if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
				$headers[$matches[1]] = $matches[2];
			}
		}

		// check for supported websocket version:
		if (!isset($headers['Sec-WebSocket-Version']) || $headers['Sec-WebSocket-Version'] < 6) {
			return static::makeHttpErrorResponse(501);
		}

		// check origin:
		//$origin = (isset($headers['Sec-WebSocket-Origin'])) ? $headers['Sec-WebSocket-Origin'] : false;
		//$origin = (isset($headers['Origin'])) ? $headers['Origin'] : $origin;

		// do handyshake: (hybi-10)
		$secKey = $headers['Sec-WebSocket-Key'];
		$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
		$response = "HTTP/1.1 101 Switching Protocols\r\n";
		$response .= "Upgrade: websocket\r\n";
		$response .= "Connection: Upgrade\r\n";
		$response .= "Sec-WebSocket-Accept: " . $secAccept . "\r\n";
		if (isset($headers['Sec-WebSocket-Protocol']) && !empty($headers['Sec-WebSocket-Protocol'])) {
			$response .= "Sec-WebSocket-Protocol: " . substr($path, 1) . "\r\n";
		}
		$response .= "\r\n";

		return new WSResponse($response, false);
	}

	/**
	 * @param int $httpStatusCode
	 * @return WSResponse
	 */
	public static function makeHttpErrorResponse($httpStatusCode = 400)
	{
		$httpHeader = 'HTTP/1.1 ';
		switch ($httpStatusCode) {
			case 400:
				$httpHeader .= '400 Bad Request';
				break;

			case 401:
				$httpHeader .= '401 Unauthorized';
				break;

			case 403:
				$httpHeader .= '403 Forbidden';
				break;

			case 404:
				$httpHeader .= '404 Not Found';
				break;

			case 501:
				$httpHeader .= '501 Not Implemented';
				break;
		}
		$httpHeader .= "\r\n";

		return new WSResponse($httpHeader, true);
	}

	/**
	 * @param $data
	 * @return WSResponse
	 */
	public static function hybi10DecodeClient($data)
	{
		$bytes = $data;
		$firstByte = sprintf('%08b', ord($bytes[0]));
		$secondByte = sprintf('%08b', ord($bytes[1]));
		$opcode = bindec(substr($firstByte, 4, 4));
		$masked = ($secondByte[0] == '1') ? true : false;
		$dataLength = ord($bytes[1]);

		if ($masked === true) {
			return static::closeResponse(1002);
		} else {
			if ($dataLength === 126) {
				$decodedData = substr($bytes, 4);
				$dataLength = bindec(sprintf('%08b', ord($data[2])) . sprintf('%08b', ord($data[3])));
			} elseif ($dataLength === 127) {
				$decodedData = substr($bytes, 10);
				$tmp = '';
				for ($i = 0; $i < 8; $i++) {
					$tmp .= sprintf('%08b', ord($data[$i + 2]));
				}
				$dataLength = bindec($tmp);
				unset($tmp);
			} else {
				$decodedData = substr($bytes, 2);
			}
		}

		return new WSResponse($decodedData, false, $opcode, $dataLength);
	}

	/**
	 * @param      $data
	 * @return WSResponse
	 */
	public static function hybi10DecodeServer($data)
	{
		$unmaskedPayload = '';
		// estimate frame type:
		$firstByteBinary = sprintf('%08b', ord($data[0]));
		$secondByteBinary = sprintf('%08b', ord($data[1]));
		$opcode = bindec(substr($firstByteBinary, 4, 4));
		$isMasked = ($secondByteBinary[0] == '1') ? true : false;
		$payloadLength = ord($data[1]) & 127;

		// close connection if unmasked frame is received:
		if ($isMasked === false) {
			return static::closeResponse(1002);
		}

		$type = WSResponse::getType($opcode);
		if ($type === null) {
			return static::closeResponse(1003);
		}

		if ($payloadLength === 126) {
			$mask = substr($data, 4, 4);
			$payloadOffset = 8;
			$dataLength = bindec(sprintf('%08b', ord($data[2])) . sprintf('%08b', ord($data[3]))) + $payloadOffset;
		} elseif ($payloadLength === 127) {
			$mask = substr($data, 10, 4);
			$payloadOffset = 14;
			$tmp = '';
			for ($i = 0; $i < 8; $i++) {
				$tmp .= sprintf('%08b', ord($data[$i + 2]));
			}
			$dataLength = bindec($tmp) + $payloadOffset;
			unset($tmp);
		} else {
			$mask = substr($data, 2, 4);
			$payloadOffset = 6;
			$dataLength = $payloadLength + $payloadOffset;
		}

		/**
		 * We have to check for large frames here. socket_recv cuts at 1024 bytes
		 * so if websocket-frame is > 1024 bytes we have to wait until whole
		 * data is transferd.
		 */
		if (strlen($data) < $dataLength) {
			return new WSResponse(null, false, $opcode);
		}

		for ($i = $payloadOffset; $i < $dataLength; $i++) {
			$j = $i - $payloadOffset;
			if (isset($data[$i])) {
				$unmaskedPayload .= $data[$i] ^ $mask[$j % 4];
			}
		}
		$payload = $unmaskedPayload;

		return new WSResponse($payload, false, $opcode, $dataLength);
	}

	/**
	 * @param        $payload
	 * @param string $type
	 * @param bool $masked
	 * @return WSResponse
	 */
	public static function hybi10Encode($payload, $type = 'text', $masked = true)
	{
		$frameHead = [];
		$payloadLength = strlen($payload);

		switch ($type) {
			case 'text':
				// first byte indicates FIN, Text-Frame (10000001):
				$frameHead[0] = 129;
				break;

			case 'close':
				// first byte indicates FIN, Close Frame(10001000):
				$frameHead[0] = 136;
				break;

			case 'ping':
				// first byte indicates FIN, Ping frame (10001001):
				$frameHead[0] = 137;
				break;

			case 'pong':
				// first byte indicates FIN, Pong frame (10001010):
				$frameHead[0] = 138;
				break;
		}

		// set mask and payload length (using 1, 3 or 9 bytes)
		if ($payloadLength > 65535) {
			$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
			$frameHead[1] = ($masked === true) ? 255 : 127;
			for ($i = 0; $i < 8; $i++) {
				$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
			}
			// most significant bit MUST be 0 (close connection if frame too big)
			if ($frameHead[2] > 127) {
				return static::closeResponse(1004);
			}
		} elseif ($payloadLength > 125) {
			$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
			$frameHead[1] = ($masked === true) ? 254 : 126;
			$frameHead[2] = bindec($payloadLengthBin[0]);
			$frameHead[3] = bindec($payloadLengthBin[1]);
		} else {
			$frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
		}
		// convert frame-head to string:
		foreach (array_keys($frameHead) as $i) {
			$frameHead[$i] = chr($frameHead[$i]);
		}
		if ($masked === true) {
			// generate a random mask:
			$mask = [];
			for ($i = 0; $i < 4; $i++) {
				$mask[$i] = chr(rand(0, 255));
			}

			$frameHead = array_merge($frameHead, $mask);
		}
		$frame = implode('', $frameHead);
		// append payload to frame:
		for ($i = 0; $i < $payloadLength; $i++) {
			$frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
		}

		return new WSResponse($frame, false);
	}

	/**
	 * @param int $statusCode
	 * @return WSResponse
	 */
	public static function closeResponse($statusCode = 1000)
	{
		$payload = str_split(sprintf('%016b', $statusCode), 8);
		$payload[0] = chr(bindec($payload[0]));
		$payload[1] = chr(bindec($payload[1]));
		$payload = implode('', $payload);
		switch ($statusCode) {
			case 1000:
				$payload .= 'normal closure';
				break;

			case 1001:
				$payload .= 'going away';
				break;

			case 1002:
				$payload .= 'protocol error';
				break;

			case 1003:
				$payload .= 'unknown data (opcode)';
				break;

			case 1004:
				$payload .= 'frame too large';
				break;

			case 1007:
				$payload .= 'utf8 expected';
				break;

			case 1008:
				$payload .= 'message violates server policy';
				break;
		}
		$payload = static::hybi10Encode($payload, 'close', false);

		return new WSResponse($payload->getResponse(), true);
	}
}